.text
.macro push(%r)
	subi	$sp, $sp, 4
	sw	%r, ($sp)
.end_macro

.macro pop(%r)
	lw	%r, ($sp)
	addi	$sp, $sp, 4
.end_macro	

main:	li	$v0, 13	    # get descriptor
	la	$a0, inputfile
	li	$a1, 0
	li	$a2, 0
	syscall 		
	move	$a0, $v0
	li	$v0, 14         # get input
	la	$a1, inputlines
	li	$a2, 100000
	syscall
    li $v0, 16          # close file
    syscall
	
	li	$t0, 0			# increment for reading memory address
	li	$t1, 1			# first index to write on
	li	$t7, 0			# starting index
	li	$s0, 0			# boolean for comments
	li	$s1, -1			# relocation
	li	$s2, 0			# line numbers
	li	$s3, 0			# memory address
	li	$s7, 0			# label offset
loop:	lbu 	$t2, inputlines($t0)
	addi	$t0, $t0, 1		# i+1
	beqz   	$t2, step2		# if null
	bgtz   	$s0, if2		# if still part of comment, check if newline
	li	$t3, 0x2a		# assign t3 to "*"
	bne	$t2, $t3, if		# check if current char ==  "*"
	lbu	$s4, inputlines($t0)		# next block converts 3 symbol octal to decimal
	subi	$s4, $s4, 0x30		# converting character to decimal val
	lbu	$s5, inputlines+1($t0)
	subi	$s5, $s5, 0x30
	lbu	$s6, inputlines+2($t0)
	subi	$s6, $s6, 0x30
	mul	$s1, $s4, 64		# multiplying by base
	mul	$s5, $s5, 8
	add 	$s5, $s5, $s6
	add	$s1, $s1,$s5		# final decimal address value
	li	$s0, 1		# assume there are comments after
	j 	loop
if:	li	$t3, 0x09		# assign t3 to tab
	beq 	$t2, $t3, loop		# jump go back to loop and check next character
	li	$t3, 0x20		# assign t3 to space
	bne	$t2, $t3, if1a		# if not a space, check if it's a comment
	lbu	$t3, inputlines($t0) # if it is a space
	beq	$t3, 0x2f, loop		# if the next char is significant, keep it
	beq	$t3, 0x09, loop		# otherwise, jump back to loop without doing anything
	beq	$t3, 0x0a, loop
	beq	$t3, $t2, loop
	sub	$t3, $t1, $t7
	beq	$t3, 1, loop
	j	keep
if1a:	li	$t3, 0x2f		# assign t3 to comment "/"
	bne	$t2, $t3, if1b		# if not a comment, check if it's a comma
	li	$s0, 1		# if it is, set boolean comments to 1 and jump back to loop
	j	loop
if1b:	li	$t3, 0x2c		# assign t3 to comma ","
	bne 	$t2, $t3, if2		# if not a comma, check if newline
	addi	$t3, $t7, 2		# if comma, start from start index and move forward
	add	$t6, $s7, $zero		# while moving label into label array in memory
	sb	$s3, labels+8($s7)		# store address of that label
	sb	$zero, labels+9($s7)		# null terminated
	addi	$s7, $s7, 10
	add	$t4, $t1, $zero
	subi	$t1, $t3, 1
whiler:	bgt	$t3, $t4, loop		# move label into label array
	lbu	$t8, offset($t3)
	sb	$t8, labels($t6)
	addi	$t3, $t3, 1
	addi	$t6, $t6, 1
	j whiler
if2:li	$t3, 0x0a		# assign t3 to newline "\n"
	bne 	$t2, $t3, if3	# if not newline, check if dollar sign
	li	$s0, 0          # reset comments
	li	$t2, 0
	beq 	$s1, -1, reset
	add	$s3, $s1, $zero
	li	$s1, -1
reset:	sub	$k0, $t1, $t7
	subi	$k0, $k0, 1
	beqz	$k0, loop
	addi	$t1, $t1, 1
	sb	$k0, offset($t7)
	sb	$t2, offset($t1)
	sb	$s3, offset+1($t7)
	addi	$s3, $s3, 1     # increment variables (start of line, end of line,
	addi	$t7, $t1, 1     # line number, mem address)
	addi	$t1, $t1,2
	addi	$s2, $s2, 1
	j	loop
if3:	bgtz	$s0, loop		# if still comment, jump to loop
	li	$t3, 0x24		# otherwise, check if $
	bne	$t2, $t3, store		# if not $, store character
	sb	$t2, offset($t1)
	j	step2
store:	bgt	$t2, 'Z', upper
keep:	addi	$t1, $t1, 1
	sb	$t2, offset($t1)
	j	loop
upper:	bgt	$t2, 'z', keep
	sub	$t2, $t2, 32		# convert small letters to capital letters
	j	keep
	
step2:	move	$t9, $s2		# number of lines, store into $t9
	li	$t0, 0			# current line counter
	li	$t1, 0			# char index
	li	$k0, 0			# max address
loop2:	beq	$t0, $t9, final
	lbu	$s0, offset($t1)	# number of chars
	lbu	$s1, offset+1($t1)	# line number
	mul	$s1, $s1, 4         # 4 characters per line - line number
	bge 	$k0, $s1, load  # change max if current is greatest
	addi	$k0, $s1, 0
load:	lbu	$s2, offset+2($t1)  # load character
	addi	$s1, $s1, 3
	li	$s4, 0x0a
	sb	$s4, outputlines($s1)   # store '\n' at end of line
	subi	$s1, $s1, 1
	blt	$s2, '0', basic     # if starts with integer,
	bgt	$s2, '9', basic     # content would be octal value
	bne	$s0, 4, basic		# convert to decimal
	lbu	$s3, offset+3($t1)
	lbu	$s4, offset+4($t1)
	lbu	$s5, offset+5($t1)
	subi	$s2, $s2, 48
	mul	$s2, $s2, 512
	subi	$s3, $s3, 48
	mul	$s3, $s3, 64
	subi	$s4, $s4, 48
	mul	$s4, $s4, 8
	subi	$s5, $s5, 48
	add	$s3, $s2, $s3
	add	$s3, $s3, $s4
	add	$s3, $s3, $s5       # final decimal value would be in bits
hex:	rem	$s4, $s3, 16    # convert bits to hex
	bge	$s4, 10, char       # if hex symbol is 'a-f'
	addi	$s4, $s4, 48
	j	writeh              # write final hex symbols to memory
char:	addi	$s4, $s4, 87
writeh:	sb	$s4, outputlines($s1)
	subi	$s1, $s1, 1
	div	$s3, $s3, 16
	rem	$s2, $s1, 4
	beq	$s2, -1, skip
	bne	$s2, 3, hex
skip:	addi	$t1, $t1, 3
	add	$t1, $t1, $s0
	addi	$t0, $t0, 1
	j	loop2

basic:	la	$a0, basicgrp		# if basic grp
    la	$a1, offset+2($t1)	    # first instruction in line
	jal 	STRSTR
	beqz 	$v0, opr1           # if output is 0, check if opr1
	lbu	$s2, 3($v0)             # load bits 9-11 for basic
	subi	$s2, $s2, 48
	sll	$s2, $s2, 11
	move	$s3, $s2
	lbu	$s2, 4($v0)
	subi	$s2, $s2, 48
	sll	$s2, $s2, 10
	add	$s3, $s3, $s2
	lbu	$s2, 5($v0)
	subi	$s2, $s2, 48
	sll	$s2, $s2, 9
	add	$s3, $s3, $s2
	lbu	$s2, offset+6($t1)      # check if indirect
	bne	$s2, 0x49, noti         # indirect basic instructions should be followed by a
	lbu	$s2, offset+7($t1)      # space, 'I', and another space
	bne $s2, 0x20, noti
	li	$s2, 1
	sll	$s2, $s2, 8
	add	$s3, $s3, $s2           # changed 8th bit to 1 if indirect
	la	$a0, labels
	la	$a1, offset+8($t1)      # if indirect
	j	checkl
noti:	la	$a0, labels
	la	$a1, offset+6($t1)      # if not indirect
checkl:	jal	STRSTR
	lbu	$s2, 8($v0)             # decimal address value of label
	add	$s3, $s3, $s2
	j	hex                     # convert to hex final bits
		
opr1:	la	$a0, opr1grp		# if grp 1 opr
op1loop:jal	STRSTR
	beqz 	$v0, opr2           # if current instruction is not part of opr 1, try opr 2
	lbu	$s2, offset+3($t1)
	bne	$s2, 0x20, opr1b        # check if there are more instructions
	la	$a1, offset+4($t1)      # if there are, check if they're also part of opr1
	j	op1loop
opr1b:	la	$a0, offset+2($t1)  # assuming opr1, check all instructions
	li	$s3, 14                 # if present and change bits accordingly
	sll	$s3, $s3, 8             # bits would be 1110 0000 0000 0000
	la	$a1, nopp		# if NOP
	jal	STRSTR
	beqz 	$v0, cla1   # if no NOP, check CLA
	j	hex             # if NOP, convert to hex
cla1:	la	$a1, cla		# if CLA, make bit 7 = 1
	jal	STRSTR
	beqz 	$v0, cll1
	li	$s2, 1
	sll	$s2, $s2, 7
	add	$s3, $s3, $s2
cll1:	la	$a1, cll		# if CLL, make bit 6 = 1
	jal	STRSTR	
	beqz 	$v0, cma1
	li	$s2, 1
	sll	$s2, $s2, 6
	add	$s3, $s3, $s2
cma1:	la	$a1, cma		# if CMA, make bit 5 = 1
	jal	STRSTR	
	beqz 	$v0, cml1
	li	$s2, 1
	sll	$s2, $s2, 5
	add	$s3, $s3, $s2
cml1:	la	$a1, cml		# if CML, make bit 4 = 1
	jal	STRSTR	
	beqz 	$v0, rartr1
	li	$s2, 1
	sll	$s2, $s2, 4
	add	$s3, $s3, $s2
rartr1: la	$a1, rar		# if RAR or RTR, make bit 3 = 1
	jal	STRSTR
	bnez  	$v0, rartr1b
	la	$a1, rtr
	jal	STRSTR
	beqz	$v0, raltl1
rartr1b:li	$s2, 1
	sll	$s2, $s2, 3
	add	$s3, $s3, $s2
raltl1:	la	$a1, ral		# if RAL or RTL, make bit 2 = 1
	jal	STRSTR
	bnez  	$v0, raltl1b
	la	$a1, rtl
	jal	STRSTR
	beqz	$v0, rtrtl1
raltl1b:li	$s2, 1
	sll	$s2, $s2, 2
	add	$s3, $s3, $s2
rtrtl1:	la	$a1, rtr		# if RTR or RTL, make bit 1 = 1
	jal	STRSTR
	bnez  	$v0, rtrtl1b
	la	$a1, rtl
	jal	STRSTR
	beqz	$v0, iac1
rtrtl1b:li	$s2, 1
	sll	$s2, $s2, 1
	add	$s3, $s3, $s2
iac1:	la	$a1, iac		# if IAC, make bit 0 = 1
	jal	STRSTR
	beqz 	$v0, hex
	li	$s2, 1
	add	$s3, $s3, $s2
	j	hex	                # conver to hex
	
opr2:	la	$a0, opr2grp        # if grp 2 opr
op2loop:jal	STRSTR
	beqz 	$v0, label          # if current instruction is not part of opr 2, try if label pointer
	lbu	$s2, offset+3($t1)
	bne	$s2, 0x20, opr2b        # check if there are more instructions
	la	$a1, offset+4($t1)      # if there are, check if they're also part of opr1
	j	op2loop
opr2b:	la	$a0, offset+2($t1)   # assuming opr1, check all instructions
	li	$s3, 15                  # if present and change bits accordingly
	sll	$s3, $s3, 8              # bits would be 1111 0000 0000 0000
	la	$a1, nopp		# if NOP, don't change bits
	jal	STRSTR
	beqz 	$v0, cla2
	j	hex             # convert to hex
cla2:	la	$a1, cla		# if CLA, make bit 7 = 1
	jal	STRSTR
	beqz 	$v0, smapa2
	li	$s2, 1
	sll	$s2, $s2, 7
	add	$s3, $s3, $s2
smapa2: la	$a1, sma		# if SMA or SPA, make bit 6 = 1
	jal	STRSTR
	bnez  	$v0, smapa2b
	la	$a1, spa
	jal	STRSTR
	beqz	$v0, snaza2
smapa2b:li	$s2, 1
	sll	$s2, $s2, 6
	add	$s3, $s3, $s2
snaza2: la	$a1, sna		# if SNA or SZA, make bit 5 = 1
	jal	STRSTR
	bnez  	$v0, snaza2b
	la	$a1, sza
	jal	STRSTR
	beqz	$v0, snlzl2
snaza2b:li	$s2, 1
	sll	$s2, $s2, 5
	add	$s3, $s3, $s2
snlzl2: la	$a1, snl		# if SNL or SZL, make bit 4 = 1
	jal	STRSTR
	bnez  	$v0, snlzl2b
	la	$a1, szl
	jal	STRSTR
	beqz	$v0, spanazlkp2
snlzl2b:li	$s2, 1
	sll	$s2, $s2, 4
	add	$s3, $s3, $s2
spanazlkp2:
	la	$a1, spa		# if SPA or SNA or SZL or SKP, make bit 3 = 1
	jal	STRSTR
	bnez  	$v0, spanazlkp2b
	la	$a1, sna	
	jal	STRSTR
	bnez  	$v0, spanazlkp2b
	la	$a1, szl	
	jal	STRSTR
	bnez  	$v0, spanazlkp2b
	la	$a1, skp
	jal	STRSTR
	beqz	$v0, hlt2
spanazlkp2b:
	li	$s2, 1
	sll	$s2, $s2, 3
	add	$s3, $s3, $s2
hlt2:	la	$a1, hlt		# if HLT, make bit 1 = 1
	jal	STRSTR
	beqz 	$v0, hex
	li	$s2, 1
	sll	$s2, $s2, 1
	add	$s3, $s3, $s2
	j	hex

label:  la	$a0, labels     # if not BASIC, GRP 1, OR GRP 2, assemble as a label pointer
	jal	STRSTR
	lbu	$s3, 8($v0)         # load the decimal address of the label
	j	hex                 # convert to hex
	
final:	li	$t0, 0
while0:	beq 	$t0, $k0, write
	lbu	$t1, outputlines($t0)
	bne	$t1, 0, incr
	rem	$t2, $t0, 4
	beq	$t2, 3, not0    # if line's character = 0, and it's the last line character, store new line '\n'
	li	$t1, 48         # otherwise, store '0'
	j	sbnew
not0:	li	$t1, 0x0a
sbnew:	sb	$t1, outputlines($t0)
incr:	addi	$t0, $t0, 1
	j	while0	

write: 	li	$v0, 13     # get descriptor
	la	$a0, outputfile
	li	$a1, 1
	li	$a2, 0
	syscall
	move	$a0, $v0
	li	$v0, 15         # write output
	la	$a1, v20raw
	addi	$k0, $k0, 12
	move	$a2, $k0
	syscall
	li	$v0, 16         # close file
	syscall
	li	$v0, 10         # end
	syscall

STRSTR:	push($s0)
	push($s1)
	push($k0)
	push($k1)
	push($t0)
	push($t1)
	push($t2)
	li	$t0, 0
while1: add	$k0, $a0, $t0
	lbu	$s0, ($k0)		#first while loop
	beqz	$s0, nomatch
	li	$t1, 0
	lbu	$s1, ($a1)
	addi	$t2, $t0, 0
while2:	beqz	$s0, next		#while loop
	beqz	$s1, next
	bne 	$s0, $s1, next
	addi	$t2, $t2, 1
	addi	$t1, $t1, 1
	add	$k0, $a0, $t2
	lbu	$s0, ($k0)
	add	$k1, $a1, $t1
	lbu	$s1, ($k1)
	j	while2			#end whle
next:	la	$t2, labels
	beqz	$s1, checks0
	beq 	$s1, 0x20, checks0	#to catch different instructions
	j	next2
checks0:bne	$a0, $t2, checkb	#if haystack is not a label, it's a match
	beqz	$s0, match		#if haystack is a label, check if 0
	beq	$t1, 8, match		#check also if at the 8th index of needle (match)
	j	next2
checkb:	la	$t2, basicgrp
	bne	$a0, $t2, match
	beq	$t1, 3, match
next2:	beq	$a0, $t2, incr10
	addi	$t0, $t0, 1
	j	while1			#endwhile
incr10:	addi	$t0, $t0, 10
	j	while1
match:	add	$v0, $a0, $t0		#if match			
	j	popstack
nomatch:li	$v0, 0			#if no match
popstack:
	pop($t2)
	pop($t1)
	pop($t0)
	pop($k1)
	pop($k0)
	pop($s1)
	pop($s0)
	jr	$ra
	
.data
offset:		.space 24
inputlines:	.space 100000
labels:		.space 1000
off:		.space 3
v20raw:		.ascii "v2.0 raw\n"
outputlines:	.space 1024
basicgrp:	.asciiz "AND000, TAD001, ISZ010, DCA011, JMS100, JMP101, OUT110"
opr1grp:	.asciiz "NOP, CLA, CLL, CMA, CML, IAC, RAR, RAL, RTR, RTL"
opr2grp:	.asciiz "NOP, HLT, CLA, SKP, SMA, SZA, SNL, SPA, SNA, SZL"
nopp:		.asciiz "NOP"
cla:		.asciiz "CLA"
cll:		.asciiz "CLL"
cma:		.asciiz "CMA"
cml:		.asciiz "CML"
iac:		.asciiz "IAC"
rar:		.asciiz "RAR"
ral:		.asciiz "RAL"
rtr:		.asciiz "RTR"
rtl:		.asciiz "RTL"
hlt:		.asciiz "HLT"
skp:		.asciiz "SKP"
sma:		.asciiz "SMA"
sza:		.asciiz "SZA"
snl:		.asciiz "SNL"
spa:		.asciiz "SPA"
sna:		.asciiz "SNA"
szl:		.asciiz "SZL"
inputfile: 	.asciiz "../io/input10.txt"
outputfile:	.asciiz "../io/output10.txt"
