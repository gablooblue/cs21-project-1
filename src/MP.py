def lines():
    f = open('../io/input10.txt')
    lines = [i.strip('\n') for i in f.readlines()]
    f.close()
    lines = [i[0:i.find('/')] if i.find('/')!=-1 else i for i in lines]
    lines = [i.strip().split(',') for i in lines]
    lines = [i for i in lines if i==[j for j in i if j]]
    lines = [[j.strip() for j in i] for i in lines]
    return lines

def assemble(instructions, group):
  out = ["0" for i in range (0,12)]
  if (group == "converted"):
    return format(int(instructions[0],8),"03x")
  elif group == "basic":
    out[0:4] = (basic[instructions[0]]) + "0"
    out[4:12] = (format(label[instructions[1].lower()], "08b"))
  elif group == "basici":
    out[0:4] = (basic[instructions[0]]) + "1"
    out[4:12] = (format(label[instructions[2].lower()], "08b"))
  elif group == "opr1":
    out[0:4] = "1110"
    if "NOP" in instructions:
      out[4:12] = "00000000"
    if "CLA" in instructions:
      out[4] = "1"
    if "CLL" in instructions:
      out[5] = "1"
    if "CMA" in instructions:
      out[6] = "1"
    if "CML" in instructions:
      out[7] = "1"
    if "RAR" in instructions or "RTR" in instructions:
      out[8] = "1"
    if "RAL" in instructions or "RTL" in instructions:
      out[9] = "1"
    if "RTR" in instructions or "RTL" in instructions:
      out[10] = "1"
    if "IAC" in instructions:
      out[11] = "1"
  elif group == "opr2":
    out[0:4] = "1111"
    if "NOP" in instructions:
      out[4:12] = "00000000"
    if "CLA" in instructions:
      out[4] = "1"
    if "SMA" in instructions or "SPA" in instructions:
      out[5] = "1"
    if "SNA" in instructions or "SZA" in instructions:
      out[6] = "1"
    if "SNL" in instructions or "SZL" in instructions:
      out[7] = "1"
    if ("SPA" in instructions or "SNA" in instructions 
    or "SZL" in instructions or "SKP" in instructions):
      out[8] = "1"
    if "HLT" in instructions:
      out[10] = "1"
  elif group == "labels":
    out[0:12] = format(label[instructions[0].lower()], "012b")
  return format(int("".join(out),2),"03x")

def identifygroup(instructions):
  length = len(instructions)
  if instructions[0] in basic:
    if instructions[1] == "I":
      return "basici"
    else:
      return "basic"
      
  opr1group = False
  for j in range(0,length):
    if instructions[j] in OPR1:
      opr1group = True
    else:
      opr1group = False
      break    
  opr2group = False
  for j in range(0,length):
    if instructions[j] in OPR2:
      opr2group = True
    else:
      opr2group = False
      break    

  if opr1group:
    return "opr1"
  elif opr2group:
    return "opr2"
  elif instructions[0].lower() in label:
    return "labels"
  else:
    return "converted"

def writeoutput(output):
  f = open('../io/output10.txt','w')
  text = "v2.0 raw\n"
  text += "\n".join(output)
  f.write(text)
  f.close()

inputlines = lines()
currentadd = 0
label = {}
basic = {"AND": "000", "TAD": "001", "ISZ" : "010", "DCA": "011", 
        "JMS": "100", "JMP": "101", "OUT": "110"}
OPR1 = {"NOP": 0, "CLA": 1, "CLL": 1, "CMA": 2, "CML": 2, "IAC": 3, "RAR": 4, "RAL": 4, "RTR": 4, "RTL": 4}
OPR2 = {"NOP": "N/A", "HLT": "N/A", "CLA": "N/A", "SKP": "N/A", "SMA": "OR", 
        "SZA": "OR","SNL": "OR", "SPA": "AND", "SNA": "AND", "SZL": "AND"}

for i in inputlines:
  if (i[0][0] == '*'):
    currentadd = int(i[0][1:4], 8)
    continue
  elif (len(i) > 1):
    label[i[0].lower()] = currentadd
  elif (i[0][0] == "$"):
    break
  currentadd += 1

output = ["" for i in range(0, currentadd)]

for i in inputlines:
  if (i[0][0] == '*'):
    currentadd = int(i[0][1:4], 8)
    continue
  elif (len(i) > 1):
    instructions = i[1].split(' ')
  else:
    instructions = i[0].split(' ')
  if instructions[0] == "$":
    break
  else:
    output[currentadd] = assemble(instructions, identifygroup(instructions))
  currentadd += 1

output = [i if i else "000" for i in output]

writeoutput(output)
